const cf = require('./PageObjects/clientFormPage');

describe('Cerebri AI Client Rep Form Testsuite', () => {
    const EC = protractor.ExpectedConditions;

    it('T1 Verify page title to be AngularDemoApp', () => {
        cf.openBrowser();
        cf.verifyPageTitle();
    });

    //Happy Case
    it('T2 Verify form can be submitted with all valid inputs', () => {
        //enter valid data
        //verifyNoErrorMessage ensures no validation errors are showing up with valid inputs
        let first_name = 'Rajat';
        let last_name = 'Bansal';
        let email = 'thisistest@gmail.com';

        cf.enterFirstName(first_name);
        cf.verifyNoErrorMessage('firstNameField');

        cf.enterLastName(last_name);
        cf.verifyNoErrorMessage('LastNameField');

        cf.selectGender('Male');

        cf.enterEmail(email);
        cf.verifyNoErrorMessage('emailField');

        cf.enterPhoneNumber('274-462-0987');
        cf.verifyNoErrorMessage('phoneNumberField');

        cf.select_qualification('Masters degree');

        cf.enterCity('Toronto');
        cf.verifyNoErrorMessage('cityFieldErr1');
        cf.verifyNoErrorMessage('cityFieldErr2');

        cf.enterState('Ontario');
        cf.verifyNoErrorMessage('stateFieldErr1');
        cf.verifyNoErrorMessage('stateFieldErr2');

        cf.enterZipCode('95122');
        cf.verifyNoErrorMessage('zipcodeFieldErr1');
        cf.verifyNoErrorMessage('zipcodeFieldErr2');
        cf.verifyNoErrorMessage('zipcodeFieldErr3');

        cf.verifySubmitButtonEnabled();
        cf.clickSubmitButton();
        //passing few arguments just to verify that the alert indeed shows few correct values
        cf.verifyAndAcceptAlert(first_name, last_name, email);
    });

    it('T3.1 Verify First Name field cannot be blank', () => {
        cf.clickResetFormButton();
        cf.clearFirstName();
        cf.verifyErrorMessage('firstNameFieldEmpty');
    });

    it('T3.2 Verify user cannot input less than 3 characters for firstname', () => {
        let invalid_first_name = 'Ra';
        cf.enterFirstName(invalid_first_name);   
        cf.verifyErrorMessage('firstNameField');
    });

    /**Actual bugs:  First Name and Last Name fields are accepting special characters and numbers as valid inputs.
    */

    it('T4.1 Verify Last Name field cannot be blank', () => {
        cf.clearLastName();
        cf.verifyErrorMessage('lastNameFieldEmpty');
    });


    it('T4.2 Verify user cannot input less than 3 characters for lastname', () => {
        let invalid_last_name = 'Ba';
        cf.enterLastName(invalid_last_name);    
        cf.verifyErrorMessage('lastNameField');
    });

    it('T5.1 Verify Email field cannot be blank', () => {
        cf.clearEmail();
        cf.verifyErrorMessage('emailFieldEmpty');
    });

    /**
     * an actual bug: Email format error doesnt show for following scenarios:
     * - When there are more than 2 characters after @ => rjt@gmail,rjt@gmail. 
     */
    it('T5.2 Verify invalid email format(missing dot) shows appropriate error message', () => {
        //let invalid_emails = ['rjt@gmail', 'rjt@gm', 'rjt', 'rjt@gmail.','rjt.'];
        let invalid_emails = ['rjt@gm', 'rjt','rjt.'];  //test will pass with these formats
        for(const invalid_email of invalid_emails){
            cf.enterEmail(invalid_email);
            cf.verifyErrorMessage('emailField');
        }
    });

    it('T6.1 Verify Phone field cannot be blank', () => {
        cf.clearPhoneNumber();
        cf.verifyErrorMessage('phoneNumberFieldEmpty');
    });

    it('T6.2 Verify invalid phone number throws appropriate error', () => {
        let invalid_phone_nums = ['6479060643', '647','647-906-abcd', '647-906','6-3-3456', '234-234-23456'];

        for(const invalid_phone_num of invalid_phone_nums) {
            cf.enterPhoneNumber(invalid_phone_num);
            cf.verifyErrorMessage('phoneNumberField');
        }
    });

    it('T7.1 Verify City field cannot be blank', () => {
        cf.clearCity();
        cf.verifyErrorMessage('cityFieldEmpty');
    });

    it('T7.2 Verify invalid city name throws appropriate error', () => {
        let invalid_cities = ['ci', 'ci123', 'city!?'];
        
        for(const invalid_city of invalid_cities){
            cf.enterCity(invalid_city);
            if(invalid_city.length < 3){
                cf.verifyErrorMessage('cityFieldErr1');
            }
            else{
                cf.verifyErrorMessage('cityFieldErr2');
            }
            
        }
    });

    it('T8.1 Verify State field cannot be blank', () => {
        cf.clearState();
        cf.verifyErrorMessage('stateFieldEmpty');
    });

    it('T8.2 Verify invalid state name throws appropriate error', () => {
        let invalid_states = ['On', 'O123', 'Ontario!!??%'];

        for(const invalid_state of invalid_states){
            cf.enterState(invalid_state);
            if(invalid_state.length < 3){
                cf.verifyErrorMessage('stateFieldErr1');
            }
            else{
                cf.verifyErrorMessage('stateFieldErr2');
            }
        }

    });

    it('T9.1 Verify Zipcode field cannot be blank', () => {
        cf.clearZipCode();
        cf.verifyErrorMessage('zipcodeFieldEmpty');
    });

    it('T9.2 Verify invalid zipcode throws appropriate error', () => {
        let invalid_zipcodes = ['9512', '951222','9512a','9512abc'];
        for(const invalid_zipcode of invalid_zipcodes) {
            cf.enterZipCode(invalid_zipcode);
            if(invalid_zipcode.length < 5){
                cf.verifyErrorMessage('zipcodeFieldErr1');
                cf.verifyErrorMessage('zipcodeFieldErr2');
            }
            else if(invalid_zipcode.length > 5){
                cf.verifyErrorMessage('zipcodeFieldErr3');
                cf.verifyErrorMessage('zipcodeFieldErr2');
            }
            else{
                cf.verifyErrorMessage('zipcodeFieldErr2');
            }
        }
    });


    it('T10 Verify Reset Button clears all fields', () => {
        //method also takes care of verifying that the form has entirely reset
        cf.clickResetFormButton(); 
    });

    
    it('T11 Verify Submit button is disabled by default', () => {
        cf.clickSubmitButton();
        cf.verifySubmitButtonDisabled();
    });

    it('T12 Verify Submit button is disabled when only few fields have been filled', () => {
        cf.clickResetFormButton();
        cf.enterFirstName('Rajat');
        cf.enterLastName('Bansal');
        cf.enterEmail('thisisatest@gmail.com');
        cf.clickSubmitButton();
        cf.verifySubmitButtonDisabled();
    });

});