exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['clientFormValidation.js'],

    onPrepare: () => {
      browser.manage().window().maximize();
    }
  };