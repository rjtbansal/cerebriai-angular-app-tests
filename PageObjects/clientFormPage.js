const clientFormPage = function() {

    const browser_url = 'http://localhost:4200/';
    const form_page_title = 'AngularDemoApp';
    const EC = protractor.ExpectedConditions;

    const first_name_input_field = element(by.xpath('//input[@ng-reflect-name = "firstName"]'));
    const last_name_input_field = element(by.xpath('//input[@ng-reflect-name = "lastName"]'));
    const gender_male_radio_button = element(by.xpath('//input[@ng-reflect-value = "male"]'));
    const gender_female_radio_button = element(by.xpath('//input[@ng-reflect-value = "female"]'));
    const email_field = element(by.xpath('//input[@ng-reflect-name = "email"]'));
    const qualification_dropdown = element(by.id('qualifications'));
    const phone_field = element(by.xpath('//input[@ng-reflect-name = "phone"]'));
    const city_field = element(by.xpath('//input[@ng-reflect-name = "city"]'));
    const state_field = element(by.xpath('//input[@ng-reflect-name = "state"]'));
    const zipcode_field = element(by.xpath('//input[@ng-reflect-name = "zipcode"]'));
    const reset_button = element(by.xpath('//button[@type="reset"]'));
    const form_cleared = element(by.css('form.ng-untouched.ng-pristine'));
    const submit_button_disabled = element(by.css('button[disabled]'));
    const submit_button = element(by.css('button[type="submit"]'));

    const validation_error_messages = {
        'firstNameField': 'First Name must be at least 3 characters',
        'firstNameFieldEmpty' : 'First Name is required',

        'lastNameField' : 'Last Name must be at least 3 characters',
        'lastNameFieldEmpty' : 'Last Name is required',

        'emailField': 'Email must match the email format',
        'emailFieldEmpty': 'Email is required',

        'phoneNumberField': 'Phone number must match the pattern ie. 123-456-7890',
        'phoneNumberFieldEmpty':'Phone number is required',

        'cityFieldErr1' : 'City should have at least 3 letters', 
        'cityFieldErr2' : 'City should not contain numeric digits',
        'cityFieldEmpty' : 'City is required',

        'stateFieldErr1' : 'State should have at least 3 letters',
        'stateFieldErr2' : 'State should not contain numeric digits',
        'stateFieldEmpty' : 'State is required',

        'zipcodeFieldErr1': 'Zipcode should have at least 5 digits',
        'zipcodeFieldErr2': 'ZipCode should have digits only ie. 12345',
        'zipcodeFieldErr3' : 'Zipcode should not have more than 5 digits',
        'zipcodeFieldEmpty' : 'Zipcode is required'
    };

    this.select_qualification = function(qualification){
        const valid_qualifications = ['Masters degree', 'Bachelors degree', 'Doctoral degree', 'Associate degree'];
        if(valid_qualifications.includes(qualification)){
            this.select_dropdown(qualification_dropdown, qualification);
        }
    };

    this.select_dropdown=function(dropdown_locator, dropdown_option){
        dropdown_locator.click();
        dropdown_locator.element(by.css(`option[value='${dropdown_option}']`)).click();
    };

    this.openBrowser = function() {
        browser.get(browser_url);
    };

    this.verifyPageTitle = function(){
        expect(browser.getTitle()).toEqual(form_page_title);
    };

    this.enterFirstName = function(first_name) {
        this.enterText(first_name, first_name_input_field);
    };

    this.clearFirstName = function(){
        this.clearText(first_name_input_field);
    }

    this.enterLastName = function(last_name) {
        this.enterText(last_name, last_name_input_field);
    };

    this.clearLastName = function(){
        this.clearText(last_name_input_field);
    }

    this.enterEmail = function(email){
        this.enterText(email, email_field);
    };

    this.clearEmail = function(){
        this.clearText(email_field);
    }

    this.enterPhoneNumber = function(phone_num) {
        this.enterText(phone_num, phone_field);
    };

    this.clearPhoneNumber = function(){
        this.clearText(phone_field);
    }

    this.selectGender = function(gender){
        gender = gender.toLowerCase();
        if(gender === 'male'){
            gender_male_radio_button.click();
        }
        else{
            gender_female_radio_button.click();
        }
    };

    this.enterCity = function(city){
        this.enterText(city, city_field);
    };

    this.clearCity = function(){
        this.clearText(city_field);
    }

    this.enterState = function(state) {
        this.enterText(state, state_field);
    };

    this.clearState = function(){
        this.clearText(state_field);
    }

    this.enterZipCode = function(zipcode) {
        this.enterText(zipcode, zipcode_field);
    };

    this.clearZipCode = function(){
        this.clearText(zipcode_field);
    }

    this.clickResetFormButton = function(){
        reset_button.click();
        browser.wait(EC.presenceOf(form_cleared), 5000);
        expect(form_cleared.isPresent()).toBe(true);
    };

    this.clickSubmitButton = function(){
        submit_button.click();
    }

    this.verifySubmitButtonDisabled = function() {
       expect(submit_button_disabled.isPresent()).toBe(true);
    }

    this.verifySubmitButtonEnabled = function() {
        expect(submit_button_disabled.isPresent()).toBe(false);
    }

    //generice function to accept and verify text in input boxes
    this.enterText = function(text, text_input_field) {
        browser.wait(EC.presenceOf(text_input_field), 5000);
        if(text_input_field.getAttribute('value') != ''){
            text_input_field.clear();
        }
        text_input_field.sendKeys(text).then(() => {
            text_input_field.sendKeys(protractor.Key.TAB);
        });
        expect(text_input_field.getAttribute('value')).toEqual(text);
    };

    this.clearText = function(text_input_field) {
       browser.wait(EC.presenceOf(text_input_field), 5000);
       text_input_field.clear().then(() => {
        text_input_field.sendKeys(protractor.Key.TAB);
       });
      
    }

    this.verifyErrorMessage = function(input_field){
        error_msg = validation_error_messages[input_field];
        error_msg_element = this.getErrorMessageElement(error_msg);
        browser.wait(EC.presenceOf(error_msg_element), 5000);
        expect(error_msg_element.isPresent()).toBe(true);
    };

    this.verifyNoErrorMessage = function(input_field){
        error_msg = validation_error_messages[input_field];
        error_msg_element = this.getErrorMessageElement(error_msg);
        expect(error_msg_element.isPresent()).toBe(false);
    };


    this.getErrorMessageElement = function(error_msg) {
        return element(by.xpath(`//*[text()[normalize-space() ='${error_msg}']]`));
    };


    this.verifyAndAcceptAlert = function(first_name, last_name, email){
        browser.wait(EC.alertIsPresent(), 5000);
        alert_text = browser.driver.switchTo().alert().getText();
        expect(alert_text).toContain(first_name);
        expect(alert_text).toContain(last_name);
        expect(alert_text).toContain(email);
        browser.switchTo().alert().accept();
    }

};

module.exports = new clientFormPage();